# Balancebeam

```

Team:
​

-   Adriel Stokes
-   Brendan Forster
-   Richard A Hofmann-Warner
-   Scott Cunningham ​

```

## Design

**Balancebeam Project Issues:**

```

-   We used Linear for issues management and project work flow.
    - https://linear.app/balance-beam/team/BAL/all

```

**Diagram of Bounded Contexts:**

```

-   Balance Beam Design
   - https://www.figma.com/file/6xMLMelT1IbLp4aVizRlTf/BalanceBeam?type=design&node-id=0-1&mode=design&t=XVve44n683h0KT1A-0

```

**Required Programs to run balancebeam:**

```

-   These programs, or their equivalents, must be installed and running in order to work with the balancebeam application:

1. Node.js
2. Docker Desktop , it is highly recommended to run balancebeam in a virtual environment.
3. A code editor such as Microsoft's Visual Code Studio
4. Upgrade your pip

```

**Instructions on downloading the balancebeam application:**
​​

```

1. Go to remote repository using this link: https://gitlab.com/hr-gamma-ramrod/balance-beam
2. Click on blue 'Code' dropdown button
3. Click on clipboard icon that copies the url corresponding to 'Clone with HTTPS'
4. Open up a terminal window and run the command: cd into whatever directory you would like to clone this project into
5. Once in that directory, run the command: git clone https://gitlab.com/hr-gamma-ramrod/balance-beam
6. Run the command: ls (to see what the name of the cloned project is)'
7. Run the command: cd in to that directory
8. Continue to next step

```

**Instructions on balancebeam application initial set-up and installation**

```

1. Start a virtual environment in your terminal
2. At the project level create a .env file in the same directory as the docker-compose.yaml.
3. Copy the information from the .env.sample to your .env file and follow all instructions. The .env file will not appear in your repository.
4. Continue to next step

```

**Setting Up Balancebeam and PG Admin PostgreSQL Databases**

```

1. Create balancebeam database in terminal with Docker command "docker volume create balancebeam"
2. Create pg-admin database in terminal with Docker command "docker volume create pg-admin"
3. If you are not currently running the required docker containers type in your termianl "docker compose up --build"
4. Open URL the local host from the docker desktop to pg-admin http://localhost:8082
5. Use the sign-in information from the env.sample for PGADMIN_DEFAULT and PGADMIN_DEFAULT_PASSWORD credentials.
6. On the pg-admin dashboard click "add new server button"
7. In the registration window, enter "balancebeam" in the name field in the general tab.
8. Click on connection tab and type "db" in the hostname field, in the username field type the username from the POSTGRES_USER_FROM_ENV, and the password from the POSTGRES_PASSWORD_FROM_ENV.
9. Hit the save button and the database is connected.
10. On left of page click on "balancebeam" dropdown and click "databases", followed by clicking the database icon called "balancebeam".
11. On the top left of the page find the "Query" tool icon and click it. In the query tool, type "SELECT \* FROM users" and hit run button.
12. You should have three columns displayed for "id", "username", and "password".
13. Repeat step #11 and type "SELECT \* FROM workouts" in the query tool and hit the run button.
14. You should have 16 columns starting from "id" and ending with "is_completed".
15. Verify all columns in both users and workouts tables match those of the migrations files in the api/migrations directory.
16. Your database is now set up to run the balancebeam application. Get Swole!

```

**High level design:**
​

```

Balancebeam uses React application, FastAPI, and a PostgreSQL database:

-   _db_ 15324:5432
-   _pgadmin_ 8082:80
-   _fastapi_ 8000:8000
-   _ghi_ 5173:5173

##User Stories

-   We want a user to sign-up up for a new account to start using the application. We have done this by creating a sign-up page and connecting that to our database.

-   The development team created the ability for a user to sign in to their account so the user can view their own account information, dashboard, and allow for a user to record a workout. This uses JWTdown authentication and a hashedpassword from the database to get a token.

-   Our design allows a user the ability to record a workout and view the user's most recent workout results in a bar chart graph on the user's dashboard. Additionally, the dashboard has a line chart that tracks the user's progress on all their own workouts.

-   We designed a leaderboard that displays the top 100 best workouts for all users for comparison.

-   Our workout page allows a user to create a new workout, record split times, tracks the number of exercises they complete, and records the final workout results all with the click of a single button. Once the workout is complete the user it redirected to their dashboard page. Additionally, a user can abort the workout, after confirming with a prompt, to stop a workout.

-   Balancebeam's database ensures that a new user must confirm their password before the creation of an account and that the new user's username has not been taken. This is done by comparing the password and verified password to the user input. A new username is compared to the database before a new user account is created.

```

## Models Description

```
​
We created the following pydantic models and placed them in the models directory. Inside the models directory we have: DuplicateUserError, Error, UserIn, UserOut, UserOutWithHashedPassword, WorkoutIn, WorkoutOut, LeaderboardOut, AccountForm, AccountToken, and HttpError.
​
Then we have three standard models for handling backend functions DuplicateUserError, Error, and HttpError:

DuplicateUserError to ensure that only one user has that same properties:

-   class DuplicateUserError(ValueError):
    _pass_

The Error returns errors when applicable

-   class Error(BaseModel):
    _message: str_

HttpError is for errors with responses

-   class HttpError(BaseModel):
    _detail: str_

Balancebeam has three models to support the user model:

UserIn that dictates how to validate a user's information.

-   class UserIn(BaseModel):
    _username: str_
    _password: st_

UserOut that dictates how a user's information is sent to a query that request information from a database.

-   class UserOut(BaseModel):
    _id: int_
    _username: str_

UserOutWithHashedPassword returns a hashed password when a user sign-up/logs-in.

-   class UserOutWithHashedPassword(UserOut):
    _hashed_password: str_

```

## Balancebeam API Endpoints

### Uses's API

| Action       | Method | URL                              |
| ------------ | ------ | -------------------------------- |
| Create Users | Post   | http://localhost:8000/api/users/ |
| Get Users    | Get    | http://localhost:8000/api/token  |

---

#### | Create a User | POST | http://localhost:8000/api/users/

​
\*To create a new user, send a JSON body following this format:\_

```

Create a user:
    Endpoint path: /api/users
    Endpoint method: POST
    Request body: JSON

    {
  "username": "string",
  "password": "string",
  "verified_password": "string"
    }

    Response: Status 200
    Response body: JSON

    {
  "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI2MGM5YzMwNS1lYmIzLTRjNmUtYTEwOS0xNGVkMjc1ZDZlYzYiLCJleHAiOjE3MDc0NDYwMjksInN1YiI6ImphbmUiLCJhY2NvdW50Ijp7ImlkIjoxMDQsInVzZXJuYW1lIjoiamFuZSJ9fQ.2sbgCobNLVLr1ZlVhl6sNQpbggchyXY01Ubk_VsEh6Q",
  "token_type": "Bearer",
  "user": {
    "id": 104,
    "username": "jane"
  }
}

```

#### | Get user token | GET | http://localhost:8000/token

```

*A token is generated by the backend with the above API call:_

```

### Authentication API

| Action       | Method | URL                             |
| ------------ | ------ | ------------------------------- |
| Create Token | Post   | http://localhost:8000/api/token |
| Delete Token | Delete | http://localhost:8000/api/token |

---

#### | Create a token | POST | http://localhost:8000/api/token

```
​
*To create a user's token, send a JSON body following this format:_



{
"access_token": "string",
"token_type": "Bearer"
}
​
```

#### | Delete a user's token | DELETE | http://localhost:8000/api/token

```

*To delete a user's token, send a JSON body following this format:_

{
true
}

```

### Workouts API

​
| Action | Method | URL |
| -------------- | -------- | ------------------------------------ |
| Create Workout | Post | http://localhost:8000/api/workouts/ |
| Get Single workout by Id | Get | http://localhost:8000/api/workouts/mine/{id} |
| Get Leaderboard | Get | http://localhost:8000/api/workouts/leaderboard |
| Get all user workouts | Get | http://localhost:8000/api/workouts/mine |

---

#### | Create a Workout | POST | http://localhost:8000/api/workouts

```

\*To create a new workout, send a JSON body following this format:\_

Create workout:
Endpoint path: /api/workouts
Endpoint method: POST
Headers: authorization: Bearer token
Request body: JSON

    {
        "date": "2024-01-11",
        "run_1": 420,
        "set_1": 420,
        "set_2": 390,
        "set_3": 380,
        "set_4": 410,
        "set_5": 425,
        "set_6": 429,
        "set_7": 520,
        "set_8": 550,
        "set_9": 555,
        "set_10": 559,
        "run_2": 450,
    }

    Response: Status 200
    Response body: JSON (integers in milliseconds)

    {
        "date": "2024-01-11",
        "run_1": 420,
        "set_1": 420,
        "set_2": 390,
        "set_3": 380,
        "set_4": 410,
        "set_5": 425,
        "set_6": 429,
        "set_7": 520,
        "set_8": 550,
        "set_9": 555,
        "set_10": 559,
        "run_2": 450,
        "user_id" 2,
        "is_completed": true
        "id": 1009,
    }

```

#### | Get Single workout by Id | GET | http://localhost:8000/api/workouts/mine/{id}

```

_To get single workout by Id, send a JSON body following this format:_

Show a single workout:
    Endpoint path: /api/workouts/{ID}
    Endpoint method: GET
    Headers: authorization: Bearer token
    Request body: JSON, no body in request

    Response: Status 200
    Response body: JSON (integers in milliseconds)

    {
        "date": "2024-02-08",
        "run_1": 10000,
        "set_1": 420,
        "set_2": 420,
        "set_3": 420,
        "set_4": 420,
        "set_5": 420,
        "set_6": 420,
        "set_7": 420,
        "set_8": 420,
        "set_9": 420,
        "set_10": 420,
        "run_2": 420,
        "user_id": 101,
        "is_completed": false,
        "id": 1009
    }

```

#### | Get Leaderboard | GET | http://localhost:8000/api/workouts/leaderboard

```

_To get the leaderboard, send a JSON body following this format:_

The leaderboard will show the best performances on the Murph workout, in descending order. Users can appear multiple times on the leaderboard, "arcade-style".

-   endpoint path: /api/workouts/leaderboard
-   Endpoint method: GET
-   Headers: authorization: Bearer token
-   Request body: n/a
-   Response body:

    MurphWorkouts: [
    {
    "date": "2024-02-08",
    "run_1": 2,
    "set_1": 2,
    "set_2": 2,
    "set_3": 2,
    "set_4": 2,
    "set_5": 2,
    "set_6": 2,
    "set_7": 2,
    "set_8": 2,
    "set_9": 2,
    "set_10": 2,
    "run_2": 2,
    "user_id": 102,
    "is_completed": true,
    "id": 1004,
    "duration": 24,
    "username": "bob"
    },
    {
    "date": "2024-02-08",
    "run_1": 2,
    "set_1": 2,
    "set_2": 2,
    "set_3": 2,
    "set_4": 2,
    "set_5": 2,
    "set_6": 2,
    "set_7": 2,
    "set_8": 2,
    "set_9": 2,
    "set_10": 2,
    "run_2": 2,
    "user_id": 102,
    "is_completed": true,
    "id": 1005,
    "duration": 24,
    "username": "bob"
    },
    {
    "date": "2024-02-08",
    "run_1": 2,
    "set_1": 2,
    "set_2": 2,
    "set_3": 2,
    "set_4": 2,
    "set_5": 2,
    "set_6": 2,
    "set_7": 2,
    "set_8": 2,
    "set_9": 2,
    "set_10": 2,
    "run_2": 2,
    "user_id": 101,
    "is_completed": true,
    "id": 1006,
    "duration": 24,
    "username": "timmy"
    },
    ]

```

#### | Get all user workouts | GET | http://localhost:8000/api/workouts/mine

```

​*To get all of a single user's workouts, send a JSON body following this format:*

List all workouts for a specific user:
Endpoint path: /api/workouts/mine
Endpoint method: GET
Headers: authorization: Bearer token
Request body: JSON, no body in request

    Return: Status 200
    Return body: JSON

MurphWorkouts: [
{
"date": "2024-02-08",
"run_1": 10000,
"set_1": 0,
"set_2": 0,
"set_3": 0,
"set_4": 0,
"set_5": 0,
"set_6": 0,
"set_7": 0,
"set_8": 0,
"set_9": 0,
"set_10": 0,
"run_2": 0,
"user_id": 101,
"is_completed": false,
"id": 1009
},
{
"date": "2024-02-08",
"run_1": 1110,
"set_1": 0,
"set_2": 0,
"set_3": 0,
"set_4": 0,
"set_5": 0,
"set_6": 0,
"set_7": 0,
"set_8": 0,
"set_9": 0,
"set_10": 0,
"run_2": 0,
"user_id": 101,
"is_completed": false,
"id": 1008
},
{
"date": "2024-02-08",
"run_1": 2,
"set_1": 2,
"set_2": 2,
"set_3": 2,
"set_4": 2,
"set_5": 2,
"set_6": 2,
"set_7": 2,
"set_8": 2,
"set_9": 2,
"set_10": 2,
"run_2": 2,
"user_id": 101,
"is_completed": true,
"id": 1006
},
]

```

```

End of Readme

```
