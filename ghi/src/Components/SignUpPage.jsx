import { useState, useEffect } from 'react'
import { useAuthContext } from '@galvanize-inc/jwtdown-for-react'
import { useNavigate, Link } from 'react-router-dom'
import registerNewUser from '../Common/registerNewUser'
import loginUser from '../Common/loginUser'
import { API_HOST } from '../main'


const SignUpPage = () => {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [verifiedPassword, setVerifiedPassword] = useState('')
    const [passwordMatchError, setPasswordMatchError] = useState('')
    const [registerError, setRegisterError] = useState('')

    const navigate = useNavigate()
    const { token, setToken } = useAuthContext()

    useEffect(() => {
        if (token) {
            navigate('/dashboard')
        }
    }, [token, navigate])

    const handleRegistration = async (e) => {
        e.preventDefault()
        setPasswordMatchError('')
        setRegisterError('')

        if (verifiedPassword != password) {
            setPasswordMatchError('Your passwords did not match')
            return
        }
        const userData = {
            username: username,
            password: password,
            verified_password: verifiedPassword,
        }
        try {
            const responseStatus = await registerNewUser(
                userData,
                `${API_HOST}/api/users`,
                setRegisterError
            )
            if (responseStatus === 200) {
                const userToken = await loginUser(
                    userData.username,
                    userData.password
                )
                setToken(userToken)
            }
        } catch (e) {
            console.error(e)
        }
    }

    return (
        <div className="flex items-center justify-center p-4">
            <div className="w-full max-w-md p-8 text-black rounded">
                <div className="text-white text-center mb-4">
                    <h2 className="text-4xl mr-4">Welcome to Balance Beam</h2>
                    <h2 className="text-4xl mr-4">Sign up here!</h2>
                </div>
                <form onSubmit={handleRegistration} className="space-y-6">
                    <div className="flex inline items-center justify-center mb-3">
                        <label className="text-5xl mr-4" htmlFor="username">
                            Username:
                        </label>
                        <input
                            name="username"
                            type="text"
                            className="form-control"
                            onChange={(e) => {
                                setUsername(e.target.value)
                            }}
                        />
                    </div>
                    <div className="flex inline items-center justify-center mb-3">
                        <label className="text-5xl mr-4" htmlFor="username">
                            Password:
                        </label>
                        <input
                            name="password"
                            type="password"
                            className="form-control"
                            onChange={(e) => {
                                setPassword(e.target.value)
                            }}
                        />
                    </div>
                    <div className="flex inline items-center justify-center mb-3">
                        <label className="text-5xl mr-4" htmlFor="username">
                            Verify Password:
                        </label>
                        <input
                            name="password"
                            type="password"
                            className="form-control"
                            onChange={(e) => {
                                setVerifiedPassword(e.target.value)
                            }}
                        />
                    </div>
                    <div>
                        <button
                            type="submit"
                            className="w-full black bg-black text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        >
                            Signup
                        </button>
                    </div>
                    {passwordMatchError ? (
                        <div
                            id="passwordsDontMatch"
                            className="text-center mt-6"
                        >
                            {passwordMatchError}
                        </div>
                    ) : (
                        ''
                    )}
                    {registerError ? (
                        <div id="registerError" className="text-center mt-6">
                            {registerError}
                        </div>
                    ) : (
                        ''
                    )}
                    <div className="text-center mt-6">
                        Already have an account?
                        <br />
                        <Link
                            className="text-orange-100 hover:text-orange-200"
                            to="/login"
                        >
                            Login Here
                        </Link>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default SignUpPage
