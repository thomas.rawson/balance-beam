import { NavLink, useNavigate } from "react-router-dom"
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react"
import useToken from "@galvanize-inc/jwtdown-for-react"


function Nav( { username } ) {
    const { token } = useAuthContext()
    const { logout } = useToken()
    const navigate = useNavigate()

    const logUserOut = (e) => {
        e.preventDefault()
        logout()
        navigate(`/`)
    }

    return (
        <nav className="flex w-full bg-custom-black items-center justify-between p-4">
            <NavLink to="/">
                <img
                    src="/balancebeam.png"
                    alt="Description"
                    className="h-20 w-20"
                />
            </NavLink>
            <div className="mr-auto flex items-start">
                <button className="button-orange">
                    <NavLink to="/instructions">The Murph</NavLink>
                </button>
                <button className="button-orange">
                    <NavLink to="/start">Start a Murph</NavLink>
                </button>
            </div>
            <div className="ml-auto flex items-end">
                {token ? (
                    <>
                        {username && <button className="button-orange">
                            <NavLink to="/dashboard">
                                Hi {username}!
                            </NavLink>
                        </button>}
                        <button className="button-orange" onClick={logUserOut}>
                            Logout
                        </button>
                    </>
                ) : (
                    <button>
                        <NavLink className="button-orange" to="/login">
                            Login
                        </NavLink>
                    </button>
                )}
            </div>
        </nav>
    )
}

export default Nav
